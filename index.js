const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('combined'))


const files = require('./src/routes/files.route');
app.use('/api/files', files);

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({message: error.message || 'Server error'});
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log('Server listen on port', PORT));
