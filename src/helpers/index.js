function getFileExtension(filename) {
  return filename.split('.').pop();
}

function isFileExtensionAllowed(extension) {
  const ALLOWED_FILE_EXTENSIONS = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

  return ALLOWED_FILE_EXTENSIONS.includes(extension);
}

module.exports = {
  getFileExtension,
  isFileExtensionAllowed,
}
