const express = require('express');
const router = express.Router();
const { body, param, validationResult } = require('express-validator');

const { isFileExtensionAllowed, getFileExtension } = require('../helpers');

const FilesController = require('../controllers/files.controller');

router.post(
  '/',
  body('filename')
    .exists()
    .withMessage("Please specify 'filename' parameter")
    .custom(value => isFileExtensionAllowed(getFileExtension(value)))
    .withMessage('Invalid file extension'),
  body('content')
    .exists()
    .withMessage("Please specify 'content' parameter"),
  validationMiddleware,
  FilesController.createFile
);

router.get('/', FilesController.getFiles);

router.get(
  '/:filename',
  param('filename')
    .custom(value => isFileExtensionAllowed(getFileExtension(value)))
    .withMessage('Invalid file extension'),
  validationMiddleware,
  FilesController.getFileByName
);

function validationMiddleware(req, res, next) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({message: errors.errors[0].msg});
  }
  
  next();
}

module.exports = router;
