const fs = require('fs');
const util = require('util');
const createError = require('http-errors');
const asyncHandler = require('express-async-handler');

const { FILES_DIRECTORY } = require('../../constants');
const { getFileExtension } = require('../helpers');

const statPromise = util.promisify(fs.stat);
const readdirPromise = util.promisify(fs.readdir);
const readFilePromise = util.promisify(fs.readFile);
const writeFilePromise = util.promisify(fs.writeFile);

module.exports = {
  createFile: asyncHandler(async (req, res) => {
    const { filename, content } = req.body;
    await writeFilePromise(`${FILES_DIRECTORY}/${filename}`, content);

    res.send({message: 'File created successfully'});
  }),
  getFiles: asyncHandler(async (req, res) => {
    const files = await readdirPromise(FILES_DIRECTORY);

    res.send({message: 'Success', files});
  }),
  getFileByName: asyncHandler(async (req, res) => {
    const { filename } = req.params;
    const files = await readdirPromise(FILES_DIRECTORY);
    
    if(files.indexOf(filename) === -1) {
      throw createError(400, `No file with ${filename} filename found`);
    }

    const { birthtime: uploadedDate } = await statPromise(`${FILES_DIRECTORY}/${filename}`);
    const content = await readFilePromise(`${FILES_DIRECTORY}/${filename}`, 'utf8');

    const extension = getFileExtension(filename);

    res.send({
      message: 'Success',
      content,
      filename,
      extension,
      uploadedDate,
    });
  }),
}
